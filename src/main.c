#include "../lib/str-to-int-arr/str-to-int-arr.h"
#include "../lib/split/split.h"
#include "../lib/slurp/slurp.h"
#include "../lib/darr/darr.h"
#include <err.h>
#include <argp.h>

DECLARE_DNLEN (int, int);
const char *argp_program_version = "fields 1.0";

enum args_flags {
	ARGS_FLAG_NO_CLAMP = 1 << 0,
	ARGS_FLAG_NO_STRIP = 1 << 1,
	ARGS_FLAG_WHOLE_STRING = 1 << 2,
	ARGS_FLAG_NO_SQUEEZE_EMPTY = 1 << 3,
	ARGS_FLAG_QUOTE = 1 << 4,
	ARGS_FLAG_COUNT = 1 << 5,
};

struct args {
	enum args_flags flags;
	struct str_arr delims;
	char *list_str;
	char *output_delim;
	struct str_arr record_delims;
	char *file;
};

static error_t parse_opt (int key, char *arg, struct argp_state *state) {
	struct args *args = (*state).input;
	switch (key) {
		case ARGP_KEY_INIT:
			*args = (struct args) {
				.delims = create_str_arr (8),
				.record_delims = create_str_arr (8),
			};
			break;
		case ARGP_KEY_ARG:
			if ((*args).file) errx (1, "Only one file allowed");
			(*args).file = arg;
			break;
		case ARGP_KEY_END:
			if (!(*args).list_str) (*args).list_str = "..";
			if (!(*args).delims.n) {
				push_str_arr (&(*args).delims, " ");
				push_str_arr (&(*args).delims, "\t");
			}
			if (!(*args).record_delims.n)
				push_str_arr (&(*args).record_delims, "\n");
			break;
		case 'c':
			(*args).flags |= ARGS_FLAG_COUNT;
			break;
		case 'C':
			(*args).flags |= ARGS_FLAG_NO_CLAMP;
			break;
		case 'd':
			push_str_arr (&(*args).delims, arg);
			break;
		case 'E':
			(*args).flags |= ARGS_FLAG_NO_SQUEEZE_EMPTY;
			break;
		case 'l':
			(*args).flags |= ARGS_FLAG_WHOLE_STRING;
			push_str_arr (&(*args).delims, "\n");
			break;
		case 'f': // fall-through
		case 'n':
			(*args).list_str = arg;
			break;
		case 'o':
			(*args).output_delim = arg;
			break;
		case 'q':
			(*args).flags |= ARGS_FLAG_QUOTE;
			break;
		case 'r':
			push_str_arr (&(*args).record_delims, arg);
			break;
		case 'S':
			(*args).flags |= ARGS_FLAG_NO_STRIP;
			break;
		case 'w':
			(*args).flags |= ARGS_FLAG_WHOLE_STRING;
			break;
		default:
			break;

	}
	return 0;
}

struct args get_args (int argc, char **argv) {
	struct args res;
	struct argp_option options[] = {
		{"count", 'c', 0, 0, "Print number of fields"},
		{"delimiter", 'd', "STRING", 0, "Field delimiter"},
		{"field", 'f', "LIST", 0, "The field[s] to print"},
		{"num", 'n', "LIST", OPTION_ALIAS},
		{"lines", 'l', 0, 0, "Same as --whole --delim=$'\n'"},
		{"output-delimiter", 'o', "STRING", 0, "Use STRING instead as the \
output delimiter. The default is to use the input delimiter"},
		{"quote", 'q', 0, 0, "Quote output. -r does nothing"},
		{"record-delim", 'r', "STRING", 0, "Divide the string up by STRING \
instead of newlines"},
		{"whole", 'w', 0, 0, "Treat the string as one record"},
		{"no-clamp", 'C', 0, 0, "Error on out-of-range list instead \
of clamping"},
		{"no-strip", 'S', 0, 0, "Don't strip whitespace"},
		{"no-squeeze-empty", 'E', 0, 0, "Retain empty fields"},
		{0},
	};
	struct argp argp = {
		options, parse_opt, 0,
		"Split a string into fields\v\
If you need to split on a newline or a tab, use your shell's $'' thing: \
\"fields -d $'\\n' <<< /file/with/newlines\"."
	};
	argp_parse (&argp, argc, argv, 0, 0, &res);
	return res;
}

static void strip (char *str) {
	char *non_whitespace = str;
	while (*non_whitespace && isspace (*non_whitespace)) non_whitespace++;

	int n_cut = non_whitespace - str;
	char *end = non_whitespace;
	int len = strlen (non_whitespace);
	end = non_whitespace + len - 1;
	while (isspace (*end)) end--;
	end++;
	memmove (str, non_whitespace, len);
	end -= n_cut;
	*end = '\0';

}

struct str_arr get_records (struct args *args) {
	struct str_arr res = {0};
	char *string = 0;
	if ((*args).file) {
		string = slurp ((*args).file);
		if (!string) err (1, "can't read %s", (*args).file);
	} else {
		string = slurp_fp (stdin);
		if (!string) err (1, "couldn't read anything");
	}
	if (!((*args).flags & ARGS_FLAG_WHOLE_STRING)) {
		res.d = split (string, 0, (*args).record_delims.d,
				(*args).record_delims.n, &res.n, 1);
		free (string);
	} else {
		res.d = malloc (sizeof *res.d);
		*res.d = string;
		res.n = res.a = 1;
	}
	return res;
}

static void print_quoted (char **fields, size_t n_fields,
		struct int_dnlen ints) {
	if (ints.n) printf ("\"%s\"", fields[*ints.d]);
	for (int *p = ints.d + 1; p < ints.d + ints.n; p++) {
		printf (" \"%s\"", fields[*p]);
	}
}

static void print (char **fields, size_t n_fields, char *output_delim,
		struct int_dnlen ints) {
	for (int *p = ints.d; p < ints.d + ints.n; p++) {
		printf ("%s", fields[*p]);

		if (p - ints.d != ints.n - 1)
			printf ("%s", output_delim);
	}
}

int main (int argc, char **argv) {

	struct args args = get_args (argc, argv);

	struct str_arr records = get_records (&args);

	for (int i = 0; i < records.n; i++) {
		size_t n_fields;
		char **fields = split (records.d[i], 0, args.delims.d,
				args.delims.n, &n_fields,
				!(args.flags & ARGS_FLAG_NO_SQUEEZE_EMPTY));
		if (!n_fields) continue;
		if (!(args.flags & ARGS_FLAG_NO_STRIP)) {
			for (char **field = fields; field < fields + n_fields; field++)
				strip (*field);
		}
		struct int_dnlen ints = {0};
		struct s2ia_info s2ia_info = s2ia_str_to_int_arr (&ints.d, &ints.n,
				args.list_str, 0, n_fields, args.flags & ARGS_FLAG_NO_CLAMP?
				0: S2IA_FLAG_ALLOW_OUT_OF_RANGE);

		// Don't try to make this cleaner. Unless you, Neil, in the future
		// are much better at programming.
		if (s2ia_info.s2ia_errno) {
			if (!(args.flags & ARGS_FLAG_NO_CLAMP)) {
				if (s2ia_info.s2ia_errno == S2IA_ERROR_NUM_TOO_BIG
						|| s2ia_info.s2ia_errno ==
						S2IA_ERROR_MINUS_NUM_TAKEN_BELOW_ZERO) {
					errx (1, "Bad list: %s\n",
							s2ia_error_strs[s2ia_info.s2ia_errno]);
				}
			} else
				errx (1, "Bad list: %s\n",
						s2ia_error_strs[s2ia_info.s2ia_errno]);
		}

		char *output_delim = args.output_delim
				? args.output_delim
				: *args.delims.d;

		if (args.flags & ARGS_FLAG_COUNT) {
			printf ("%zu\n", ints.n);
			exit (0);
		}
		if (!(args.flags & ARGS_FLAG_QUOTE))
			print (fields, n_fields, output_delim, ints);
		else
			print_quoted (fields, n_fields, ints);
		putchar ('\n');
		for (char **field = fields; field < fields + n_fields; field++)
			free (*field);
		free (fields);
		free (ints.d);
	}
	exit (0);
}
