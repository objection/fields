# Fields

An alternative to cut and awk '{print $1}'.

One difference to cut is this program's lists are the more modern
style. I think about it as Ruby's style. Dunno if it was invented
there.

Examples: "0..3" ([1, 2, 3]), "0.." (0 to n fields) ".." (same),
"..-2", (0 to the second-to-last).

Another difference is you split on a string, not just a single char.

Another difference is you can specify multiple delimiters with
repeated invocations of --delimiter.

Another difference is (for now, at least), fields are stripped of
whitespace by default. Use --no-strip to retain whitespace.

Like cut, by default the output-delimiter is your input delimiter. If
you split on more than one delimiter, the output delimiter is the
first one.

## Examples 

Change ":"s in /etc/passwd to spaces.
```
fields /etc/passwd -d $'\n' -f.. | fields -d: -f.. -o' '
```

lsblk | grep loop0 | field -f-1
``` 
echo "root's shell is $(fields -d: -f -1 <<< $(grep '^root' /etc/passwd))"
```

## Bugs

There's a memory bug I can't figure out. Invalid read, says Valgrind.


