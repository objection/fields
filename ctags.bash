#!/usr/bin/env bash

shopt -s globstar

gcc -M lib/**/*.[ch] src/**/*.[ch] | sed -e 's/[\\ ]/\n/g' | \
        sed -e '/^$/d' -e '/\.o:[ \t]*$/d' | \
		ctags -L - 

